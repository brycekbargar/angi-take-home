# Angi Take Home (birb edition)

## Running the Application

#### Get a token to the [Cornell eBird API](https://ebird.org/data/download).

I also sent along mine with the submission.

#### Setup a local k8s Cluster

A quick aside about my dev setup. I'm using Docker Desktop on Windows.
All my development happens inside a linux container.
/var/run/docker.sock is bound into this container so 99% of docker cli commands "just work".

Networking is the exception.
For k8s, minikube would not run when started from the dev container for networking related reasons.
Fortunately Docker Desktop ships with a single node k8s cluster! This is what I'm using.

To setup k8s
1. First I followed the [setup guide in Windows](https://docs.docker.com/desktop/kubernetes/).
2. Then I used `docker cp C:\Users\katherine\.kube\config stable-dev-container:/tmp` to load the connection information into the dev container.
3. After that, any non-networking related kubectl commands worked from inside the dev container.

#### Setup Minio

For this practical I used the toy installation option of the [community helm chart](https://github.com/minio/minio/tree/master/helm/minio).

```shell
kubectl create namespace minio

helm repo add minio https://charts.min.io/
helm install --namespace minio \
    --set resources.requests.memory=512Mi \
    --set replicas=1 \
    --set persistence.enabled=false \
    --set mode=standalone \
    --set rootUser=rootuser,rootPassword=rootpass123 \
    --generate-name \
    minio/minio

kubectl get pods --namespace minio
```

Save the rootUser and rootPassword (this is terribly insecure).
Also save the name of the Minio pod (not the post-job one).

Weirdly, the post-job container never actually started but seemed to be fine for me?

This next piece exposes the Minio UI and API.
I had to do it on Windows (see networking caveat above) but it could also be done wherever you were running the previous commands.

```powershell
kubectl.exe port-forward --namespace minio minio-1705779356-957949f4c-d4f5m 9000 &
kubectl.exe port-forward --namespace minio minio-1705779356-957949f4c-d4f5m 9001 &
```

Open the [Minio UI](http://localhost:9001) and login with the rootUser credentials.
Create a bucket in the UI and save the name of the bucket. This could be automated.

```shell
kubectl run <ANY_POD_NAME> \
    --restart=Never \
    --image=registry.gitlab.com/brycekbargar/angi-take-home \
    --env="EBIRDTOKEN=<YOUR_TOKEN_FROM_EARLIER>" \
    --env="MINIOENDPOINT=host.docker.internal:9000" \
    --env="MINIONOSECURE=1" \
    --env="MINIOACCESSKEY=rootuser" \
    --env="MINIOSECRETKEY=rootpass123" \
    -- <YOUR_BUCKET_FROM_EARLIER>
```

Replace the MINIO* values with whatever matches your setup. This is terribly insecure.
host.docker.internal is a special url that resolves to the Docker Desktop host network inside containers.

## Contributing

### System Requirements

System level requirements are in the environment.yml file.
If you are using conda you can do `conda env create -f environment.yml`
Otherwise this repository requires python and poetry.

From the angi directory, `poetry install` will get you all the dependencies.

### Development

I've used [JupyterLab](https://jupyter-docker-stacks.readthedocs.io/en/latest/index.html) + [JupyText](https://jupytext.readthedocs.io/en/latest/index.html) to develop the application more quickly.
If you intend to make changes you will likely want to do the same.
This isn't a super scalable pattern but it is quick!

### Pre-Commit Hooks and CI

CI is setup to lint and check formatting using [Ruff](https://docs.astral.sh/ruff/) and type check using mypy, which are installed using poetry.
You'll likely want to use a pre-commit library to run these tools, I personally use [Precious](https://github.com/houseabsolute/precious).

### Deployment

Any push to the main branch will create a new image with two tags:
* latest (this is bad practice to use in production but makes dev convenient)
* short git hash (use this for reproduceable deploys!)

## Future Enhancements

### Partitioning

Partitioning was called out the requirements doc. There are a number of reasons why I skipped it
1. I wanted to make sure I got through the CI/CD piece
1. All the data is loaded into memory, this is the limiting factor for large datasets currently
1. Any partitioning scheme would feel contrived without knowing the downstream consumer requirements

I _do_ have an example of where I have partitioned parquet [for a side project](https://github.com/brycekbargar/si-tools/blob/main/pipelines/sugr_games.py).

The initial implementation had a quick feedback cycle because it was so simple.
To support partitioning and large datasets, first I'd spend time building better integration tests to keep the feedback cycle quick.

### Secrets management
This practical plays fast and loose with secrets.
Aside from the eBird token, everything is a local dev secret which makes it only slightly less bad.

At work for secrets management we're using
* Vault + Helm TF Providers to do secrets at deploy time
* [Vault Sidecar Injector](https://developer.hashicorp.com/vault/docs/platform/k8s/injector) to do secrets at run time
* k8s Services Accounts to sidestep as much secrets management as possible

An example of the last thing for Minio is their [STS token support](https://min.io/docs/minio/kubernetes/upstream/developers/sts-for-operator.html).

### Deployment

In my experience, it is rare to run ETL directly on k8s.
There is usually a higher level orchestration layer, for example Ray.io or Airflow or Metaflow or....

A simple [CronJob manifest](https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/) could have been included for this practical.
In order to make that work, I'd have to setup k8s secrets to avoid hardcoding the keys into the manifest.
