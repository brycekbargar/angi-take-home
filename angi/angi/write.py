# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.16.0
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
# %mamba install minio pyarrow

# %%
"""Methods agnostic to birds, but useful for uploading birds to minio.
"""

# %%
import io
import typing

import minio
import pyarrow as pa
import pyarrow.parquet as pq


# %%
def create_parquet_stream(table: pa.Table) -> tuple[typing.BinaryIO, int]:
    """Write PyArrow Tables into a stream as parquet.

    Args:
        table: An arbitrary PyArrow Table.

    Returns:
        A tuple of the parquet stream + the length of the parquet stream.
    """

    p_buf = pa.BufferOutputStream()
    pq.write_table(table, p_buf)
    b = p_buf.getvalue()
    return (io.BytesIO(b.to_pybytes()), b.size)


# %%
if hasattr(__builtins__, "__IPYTHON__"):
    import os

    from .bird_data import bird_info, birds_as_table

    birds_json = bird_info(os.environ["EBIRDAPITOKEN"])
    birds = birds_as_table(birds_json)


# %%
def minio_put_object(
    client: minio.Minio,
    bucket: str,
    object_name: str,
    object_data: typing.BinaryIO,
    length: int = -1,
):
    """Uploads a single streamed object to a bucket using the provided client.

    Args:
        client: A Minio python client instance.
        bucket (str): The target bucket.
        object_name (str): The key for the object in the target bucket.
        object_data (stream): The object as a binary stream to be uploaded.
        length (int): The (optional) length of the binary stream.
            If no size is given the object will be uploaded in 10Mb chunks.

    Returns:
        The Minio upload response.
    """

    if length == -1:
        return client.put_object(
            bucket, object_name, object_data, -1, part_size=10 * 1024 * 1024
        )
    return client.put_object(bucket, object_name, object_data, length)


# %%
if hasattr(__builtins__, "__IPYTHON__"):
    client = minio.Minio(
        "play.minio.io:9000",
        access_key="Q3AM3UQ867SPQQA43P2F",
        secret_key="zuf+tfteSlswRu7BJ86wekitnifILbZam1KYY3TG",
    )

# %%
if hasattr(__builtins__, "__IPYTHON__"):
    client = minio.Minio(
        "host.docker.internal:9000",
        secure=False,
        access_key="rootuser",
        secret_key="rootpass123",
    )

# %%
if hasattr(__builtins__, "__IPYTHON__"):
    res = minio_put_object(
        client, "test-bucket", "birds.parquet", *create_parquet_stream(birds)
    )
    print(res)

# %%
