# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.16.0
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
# %mamba install requests pyarrow

# %%
"""Methods specific to downloaded and parsing data from the Cornell eBird API.

You can learn more and get your own token at: https://ebird.org/data/download.
"""

# %%
import typing

import pyarrow as pa
import requests


# %%
def bird_info(eBirdToken: str) -> list[dict[str, typing.Any]]:
    """Return the last 30 days of birding observations in the United States.

    Args:
        eBirdToken (str): Token for the eBird API.

    Returns:
        A list of birding observations. For example:

        {"speciesCode": "hoocro1",
         "comName": "Hooded Crow",
         "sciName": "Corvus cornix",
         "locId": "L7884500",
         "locName": "Ahmet Yesevi University",
         "obsDt": "2020-01-21 16:35",
         "howMany": 1,
         "lat": 43.530936,
         "lng": 79.455132,
         "obsValid": true,
         "obsReviewed": false,
         "locationPrivate": true,
         "subId": "S63619695"}
    """

    return requests.get(
        "https://api.ebird.org/v2/data/obs/US/recent",
        params={"back": 30},
        headers={"X-eBirdApiToken": eBirdToken},
    ).json()


# %%
def birds_as_table(birds_json: list[dict[str, typing.Any]]) -> pa.Table:
    """Convert a list of birding observations into a PyArrow Table.

    Args:
        birds_json (list): List of observations,
            probably retrieved with :meth:`bird_info`.

    Returns:
        A PyArrow Table of the passed in data with some type fixes.
    """

    birds_schema = pa.schema(
        [
            pa.field("speciesCode", pa.string()),
            pa.field("comName", pa.string()),
            pa.field("sciName", pa.string()),
            pa.field("locId", pa.string()),
            pa.field("locName", pa.string()),
            pa.field("obsDt", pa.string()),
            pa.field("howMany", pa.int16()),
            pa.field("lat", pa.float64()),
            pa.field("lng", pa.float64()),
            pa.field("obsValid", pa.bool_()),
            pa.field("obsReviewed", pa.bool_()),
            pa.field("locationPrivate", pa.bool_()),
            pa.field("subId", pa.string()),
        ]
    )

    birds = pa.Table.from_pylist(birds_json, schema=birds_schema)
    return birds


# %%
if hasattr(__builtins__, "__IPYTHON__"):
    import os

    birds_json = bird_info(os.environ["EBIRDAPITOKEN"])
    birds = birds_as_table(birds_json)
    print(birds.num_rows)
    print(birds)

# %%

# %%

# %%
