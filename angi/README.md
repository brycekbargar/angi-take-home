# Angi

CLI application for ~~all your~~ some very specific birb loading needs.

## Usage

```
usage: python main.py [-h] [--token TOKEN] [--minio-endpoint MINIO_ENDPOINT] [--minio-nosecure] [--minio-accesskey MINIO_ACCESSKEY] [--minio-secretkey MINIO_SECRETKEY] bucket

Downloads birds from Cornell eBird API and uploads them as Parquet to Minio.

positional arguments:
  bucket                The target bucket for bird upload (must already exist)

options:
  -h, --help            show this help message and exit
  --token TOKEN         Token for the eBird API, also reads EBIRDTOKEN
  --minio-endpoint MINIO_ENDPOINT
                        Endpoint for the Minio server, also reads MINIOENDPOINT
  --minio-nosecure      Upload over insecure connection, also reads MINIONOSECURE
  --minio-accesskey MINIO_ACCESSKEY
                        Access Key for the Minio server, also reads MINIOACCESSKEY
  --minio-secretkey MINIO_SECRETKEY
                        Secret Key for the Minio server, also reads MINIOSECRETKEY

🦃🐔🐓🐣🐤🐥🐦🐧🕊️🦅🦆🦢🦉🦤🦩🦚🦜🪿
```
