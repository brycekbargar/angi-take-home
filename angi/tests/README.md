# Where are the tests?

### Short answer

There are none.

### Long answer

Tests didn't feel necessary (or helpful, yet...) for <100 very inspectable lines.

I _do_ really like test driven development as a way to shorten feedback cycles, especially for compiled languages like go or C#.
Python gives you access to Jupyter Notebooks, which this project took full advantage of.
You can find the "tests" I used to go fast and verify things next to the code in the notebooks.

Critically, I don't know how this code is being consumed.
Tests are another (and often the first) consumer of your application and good ones reflect the intended usage.

For some examples of my testing work in other projects see:
* [Testing the round-tripability of domain objects in a storage agnostic way](https://github.com/brycekbargar/golang-backend/blob/main/adapters/testcases/users.go)
* [Testing the top level contract and doubling as documentation](https://github.com/brycekbargar/steelseries_ffmpeg/blob/main/ffmpeg/command_test.go)
