import argparse
import os

from minio import Minio

import angi.bird_data as bd
import angi.write as bw


def main(bucket: str, eBirdToken: str, client: Minio):
    birds_json = bd.bird_info(eBirdToken)
    birds = bd.birds_as_table(birds_json)

    (stream, length) = bw.create_parquet_stream(birds)
    bw.minio_put_object(client, bucket, "birds.parquet", stream, length)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="python main.py",
        description=(
            "Downloads birds from Cornell eBird API "
            "and uploads them as Parquet to Minio."
        ),
        epilog="🦃🐔🐓🐣🐤🐥🐦🐧🕊️🦅🦆🦢🦉🦤🦩🦚🦜🪿",
    )
    parser.add_argument(
        "bucket", help="The target bucket for bird upload (must already exist)"
    )
    parser.add_argument(
        "--token",
        default=os.environ.get("EBIRDTOKEN"),
        help="Token for the eBird API, also reads EBIRDTOKEN",
    )
    parser.add_argument(
        "--minio-endpoint",
        default=os.environ.get("MINIOENDPOINT"),
        help="Endpoint for the Minio server, also reads MINIOENDPOINT",
    )
    parser.add_argument(
        "--minio-nosecure",
        action="store_true",
        default=os.environ.get("MINIONOSECURE") is not None,
        help="Upload over insecure connection, also reads MINIONOSECURE",
    )
    parser.add_argument(
        "--minio-accesskey",
        default=os.environ.get("MINIOACCESSKEY"),
        help="Access Key for the Minio server, also reads MINIOACCESSKEY",
    )
    parser.add_argument(
        "--minio-secretkey",
        default=os.environ.get("MINIOSECRETKEY"),
        help="Secret Key for the Minio server, also reads MINIOSECRETKEY",
    )

    args = parser.parse_args()
    client = Minio(
        args.minio_endpoint,
        secure=(not args.minio_nosecure),
        access_key=args.minio_accesskey,
        secret_key=args.minio_secretkey,
    )
    main(args.bucket, args.token, client)
